# I2C_TOOLS

#### 介绍
安卓I2C的调试工具
将i2_tools直接放入安卓的externals目录
然后mmm external/i2c_tools即可。



#### 使用说明

1、用i2cdetect检测有几组i2c总线在系统上，输入：
./i2cdetect -l
2、用i2cdetect检测挂载在i2c总线上器件，输入
 ./i2cdetect-r -y 1（检测i2c-1上的挂载情况
3、用i2cdump查看器件所有寄存器的值，这个很有用，输入
 ./i2cdump -f -y 1 0x20 （查看adv7401寄存器值）
4、用i2cset设置单个寄存器值，用i2cget读取单个寄存器值，可以在初期调试时发挥很大作用，一旦有预期的现象出现，就可以用i2cdump读出整个寄存器的值，然后固化到代码中。
上图是i2cset和i2cget使用方法：
./i2cset -f -y 1 0x20 0x77 0x3f （设置i2c-1上0x20器件的0x77寄存器值为0x3f）
./i2cget -f -y 1 0x20 0x77     （读取i2c-1上0x20器件的0x77寄存器值）
上图是i2cset和i2cget使用方法：
rk3399pro:/data/i2c_tools # ./i2cdetect  -r -y 1
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- --
10: -- -- -- -- -- -- -- -- -- -- -- -- UU -- -- --
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
50: 50 51 -- -- -- -- -- -- -- -- -- -- -- -- -- --
60: -- -- -- -- -- -- -- -- 68 -- -- -- -- -- -- --
70: -- -- -- -- -- -- -- --
#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
